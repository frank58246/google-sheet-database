require('dotenv').config();
const express = require('express');
const app = express();
const { google } = require('googleapis');
const auth = new google.auth.GoogleAuth({
  keyFile: 'credentials.json',
  scopes: 'https://www.googleapis.com/auth/spreadsheets'
});
const spreadsheetId = '17KFy9_LX4OiYBx4LQnKN0v0gFWIZct0fJ_qDrqc0-uo'

function getAuth() {
  const auth = new google.auth.GoogleAuth({
    keyFile: 'credentials.json',
    scopes: 'https://www.googleapis.com/auth/spreadsheets',
  });
  return auth;
}

// proccure googleSheet method
async function getGoogleSheet(auth) {
  const client = await auth.getClient();
  const googleSheet = google.sheets({ version: 'v4', auth: client });
  return googleSheet;
}

app.listen(3000 || process.env.PORT, () => {
  console.log('Up and running!!');
});

app.get('/', async (req, res) => {
  const auth = getAuth();
  const googleSheet = await getGoogleSheet(auth);

  // const getMetaData = await googleSheet.spreadsheets.get({
  //   auth,
  //   spreadsheetId,
  // });

  const getSheetData = await googleSheet.spreadsheets.values.get({
    auth,
    spreadsheetId,
    range: '工作表1!A2:B',
  });

  res.send(getSheetData.data.values);
});